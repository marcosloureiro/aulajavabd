package TesteCon;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class testeCon {

	public static void main(String[] args) {
		
		try {
            
            // 1. Carregar o driver
            Class.forName("com.mysql.jdbc.Driver");
            
            // 2. Definir a URL de conex�o
            String strCon = "jdbc:mysql://162.241.203.20:3306/infinito_uni";
            
            // 3. Estabelecer a conex�o
	        Connection con = DriverManager.getConnection(strCon, "infinito_uni", "admin@001");
            
            //4. Criar objeto do tipo statement
            Statement stmt = con.createStatement();
            
            //5. Executar uma consulta
            int resultado = stmt.executeUpdate("INSERT INTO banco (numeroConta, saldo, tipo) VALUES (2120, '50', 'corrente');");
            
            if(resultado > 0) {
            	System.out.println("Inserido com sucesso!");
            }
            ResultSet rs = stmt.executeQuery("SELECT * FROM banco");
            //6. Processar resultado
            
            while (rs.next()){
                  System.out.println("N�mero: " + rs.getString("numeroConta") 
                  + " - " + "Saldo: " + rs.getShort("saldo"));
            }
            
            //7. Fechar conex�o
            con.close();
            rs.close();
            stmt.close();
            
        } catch (ClassNotFoundException ex1) {
            System.out.println("Classe n�o encontrada");
        } catch (com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException ex2) {
        	System.out.println("Medico ja cadastrado!");
        } catch (SQLException ex3) {
        	System.out.println(ex3);
        }

	}

}
