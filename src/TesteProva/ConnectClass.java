package TesteProva;
import java.sql.*;

public class ConnectClass {

	public static void main(String[] args) throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");//t
			
			String strCon = "jdbc:mysql://localhost:3306/banco";

	        Connection con = DriverManager.getConnection(strCon, "root", "root");
	        
	        //*************************************
	        //STATEMENT SOMENTE
	        //STATEMENT N�O PEDE A STRING SQL NA CRIA��O DO OBJETO
	        
	        Statement stmt = con.createStatement();

	        
	        //CONSULTA
	        //TIPO DE RETORNO OBJETO ResultSet
	        
	        String query = "SELECT FROM WHERE";
	        ResultSet rs = stmt.executeQuery(query); //EXECUTE QUERY - RETORNA OBJETO
	        
	        while (rs.next()){
                //rs.getString("nome");
               // rs.getShort("idade");
	        }
	        
	        //ALTERA��O
	        //TIPO RETORNO INT COM AS LINHAS AFETADAS
	        
	        String query2 = "UPDATE FROM SET";
	        int rs2 = stmt.executeUpdate(query2); //EXECUTE QUERY - RETORNA OBJETO
	        
	        if(rs2 > 0) {
	        	System.out.println("Linhas alteradas: " + rs2);
	        }else {
	        	System.out.println("Nenhuma Altera��o realizada");
	        }
	    	        
	    	//**************************************
	    	//PREPARE STATEMENT
	        //PREPARA E DEPOIS EXECUTA (MUITAS VEZES)
	        //PREPARE PEDE A STRING DE SQL NA CRIA��O DO OBJETO
	        
	        String query3 = "SELECT FROM WHERE ? AND ?";
	        PreparedStatement pstmt = con.prepareStatement(query3); //IGUAL PARA CONSULTA E UPDATE
	        
	        //PREPARA AS VARIAVIES
	        //CONSULTA
	        pstmt.setString(1, "Unfogging the Future"); // PRIMEIR A POSI��O E SEGUNDO O VALOR
	        pstmt.setInt(2, 1000);

	        //TIPO DE RETORNO OBJETO ResultSet
	        ResultSet rs4 = pstmt.executeQuery(); //AQUI � DIFERENTE PARA CONSULTA E UPDATE
	        
	        while (rs4.next()){
                //rs.getString("nome");
               // rs.getShort("idade");
	        }
	        

	        //ALTERA��O
	        
	        String query4 = "DELETE FROM WHERE ?";
	        PreparedStatement pstmt2 = con.prepareStatement(query4); //IGUAL PARA CONSULTA E UPDATE
	        
	        pstmt2.setInt(1, 1);
	        pstmt2.setInt(1, 2);

	        
	        //TIPO RETORNO INT COM AS LINHAS AFETADAS
	        int delnum = pstmt.executeUpdate();
	        
	        if(delnum > 0) {
	        	System.out.println("Linhas APAGADAS: " + delnum);
	        }else {
	        	System.out.println("Nenhuma Altera��o realizada");
	        }

			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
