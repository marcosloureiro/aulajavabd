package clinica;

import java.sql.*;

import javax.swing.JOptionPane;

public class testeADD {
	static String valor1 = "Marcos";
	static String valor2 = "010011111";

	public static void main(String[] args)
	  {
	    try
	    {
	      // create a mysql database connection
	      String myDriver = "org.gjt.mm.mysql.Driver";
          String strCon = "jdbc:mysql://localhost:3306/clinica";
	      Class.forName(myDriver);
	      Connection conn = DriverManager.getConnection(strCon, "root", "root");
	    
	      // create a sql date object so we can use it in our INSERT statement

	      // the mysql insert statement
	      String query = "INSERT INTO medicos (codm,nome,idade,especialidade,CPF,cidade,nroa)"
	      		+ "VALUES (20,?,40,'ortopedia',10110100000,'Florianopolis',?)";
	      

	      // create the mysql insert preparedstatement
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, "Marcos");
	      preparedStmt.setString (2, "2");

	      // execute the preparedstatement
	      preparedStmt.execute();
	      System.out.println("Inserido com sucesso!");
	      
	      conn.close();
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	    }
	  }

}
