package clinica;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import java.awt.Choice;

public class CRUDjava {

	private JFrame frame;
	private JTextField txtMatricula;
	private JTextField txtNome;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CRUDjava window = new CRUDjava();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CRUDjava() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblMatrcula = new JLabel("Matr\u00EDcula");
		lblMatrcula.setBounds(10, 11, 120, 14);
		frame.getContentPane().add(lblMatrcula);
		
		txtMatricula = new JTextField();
		txtMatricula.setBounds(10, 36, 120, 20);
		frame.getContentPane().add(txtMatricula);
		txtMatricula.setColumns(10);
		
		JLabel lblNomeDoMdico = new JLabel("Nome do m\u00E9dico");
		lblNomeDoMdico.setBounds(10, 80, 219, 14);
		frame.getContentPane().add(lblNomeDoMdico);
		
		txtNome = new JTextField();
		txtNome.setBounds(10, 105, 219, 20);
		frame.getContentPane().add(txtNome);
		txtNome.setColumns(10);
		
		JButton btnIncluir = new JButton("Incluir");
		btnIncluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
				      // create a mysql database connection
				      String myDriver = "org.gjt.mm.mysql.Driver";
			          String strCon = "jdbc:mysql://localhost:3306/clinica";
				      Class.forName(myDriver);
				      Connection conn = DriverManager.getConnection(strCon, "root", "root");
				    
				      // create a sql date object so we can use it in our INSERT statement

				      // the mysql insert statement
				      String query = "INSERT INTO medicos (codm,nome,idade,especialidade,CPF,cidade,nroa)"
				      		+ "VALUES (20,?,40,'ortopedia',10110100000,'Florianopolis',?)";
				      

				      // create the mysql insert preparedstatement
				      PreparedStatement preparedStmt = conn.prepareStatement(query);
				      preparedStmt.setString (1, txtNome.getText());
				      preparedStmt.setString (2, txtMatricula.getText());

				      // execute the preparedstatement
				      preparedStmt.execute();
				      JOptionPane.showMessageDialog(btnIncluir, "Inserido com sucesso!");
				      
				      conn.close();
					
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});
		btnIncluir.setBounds(335, 11, 89, 23);
		frame.getContentPane().add(btnIncluir);
		
		Choice choice = new Choice();
		choice.add("Green");
		choice.add("Blue");
		choice.setBounds(10, 143, 219, 20);
		frame.getContentPane().add(choice);
	}
}
