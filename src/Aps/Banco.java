package Aps;

import java.util.*;

public class Banco {
	private List<ContaBancaria> Contas = new ArrayList<ContaBancaria>();
	
	public void inserir(ContaBancaria conta) {
		Contas.add(conta);
	}
	public void remover(long numero) {
		ContaBancaria p = procurarConta(numero);
		if(p.getNumeroConta() != 0) {
			Contas.remove(Contas.indexOf(p));
		}
	}
	public boolean existeConta(long numeroConta){
		for (ContaBancaria p: Contas) {
			if(p.getNumeroConta() == numeroConta) {
				return true;
			}
		}
		return false;
	}
	public ContaBancaria procurarConta(long numeroConta){
		for (ContaBancaria p: Contas) {
			if(p.getNumeroConta() == numeroConta) {
				return p;
			}
		}
		return null;
	}
	
	public List<ContaBancaria> getContas() {
		return Contas;
	}
	public void setContas(List<ContaBancaria> contas) {
		Contas = contas;
	}
	public void listarContas() {
		for (ContaBancaria p: Contas) {
			
		}		
	}
}
