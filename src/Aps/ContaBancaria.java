package Aps;

public abstract class ContaBancaria {
	private long numeroConta;
	private double saldo;
	
	public ContaBancaria(long numeroConta, double saldo) {
		this.numeroConta = numeroConta;
		this.saldo = saldo;
	}
	
	public void sacar(double valor) {
		saldo = saldo - valor;
	}
	
	public void depositar(double valor) {
		saldo = saldo + valor;		
	}
	
	public void transferir(double valor, ContaBancaria ContaDe, ContaBancaria ContaPara) {
		ContaDe.sacar(valor);
		ContaPara.depositar(valor);
	}
	
	public long getNumeroConta() {
		return numeroConta;
	}
	
	public void setNumeroConta(long numeroConta) {
		this.numeroConta = numeroConta;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
}
