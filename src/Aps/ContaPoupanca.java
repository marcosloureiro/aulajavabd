package Aps;

public class ContaPoupanca extends ContaBancaria implements Imprimivel {
	private double limite = 500.0;

	public ContaPoupanca(long numeroConta, double saldo) {
		super(numeroConta, saldo);
	}
			
	public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}

	public void mostrarDados() {
		
	}
	
}
