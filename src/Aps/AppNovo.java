package Aps;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JSeparator;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JTable;
import java.awt.Color;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.DefaultComboBoxModel;

public class AppNovo extends JFrame {

	private JPanel contentPane;
	private JTextField n_conta2;
	private JTextField valor_2;
	private JTextField conta_para;
	private JTextField n_conta1;
	private JTextField valor_1;
	private JTable table;
	private DefaultTableModel modelo = new DefaultTableModel();
	private String[] objRow = new String[2];
	private static Locale locale  = new Locale("pt", "BR");
	private static DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
	private static String pattern = "###,###.00";
	private static DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
	private JLabel msg;
	//private static List<ContaBancaria> ContasNew = new ArrayList<ContaBancaria>();
	//private MaskFormatter mascaraValor = new MaskFormatter("000");

	private static Banco banco = new Banco();
	private JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppNovo frame = new AppNovo();
					frame.setVisible(true);
					ContaCorrente C1 = new ContaCorrente(2526, 120.0);
					ContaCorrente C2 = new ContaCorrente(8926, 1120.15);
					ContaPoupanca C3 = new ContaPoupanca(2126, 540.0);
					ContaCorrente C4 = new ContaCorrente(2527, 790.0);
					banco.inserir(C1);
					banco.inserir(C2);
					banco.inserir(C3);
					banco.inserir(C4);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AppNovo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 425);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNDaConta = new JLabel("N\u00BA da Conta");
		lblNDaConta.setHorizontalAlignment(SwingConstants.LEFT);
		lblNDaConta.setBounds(11, 153, 88, 14);
		contentPane.add(lblNDaConta);
		
		n_conta2 = new JTextField();
		n_conta2.setBounds(11, 170, 86, 20);
		contentPane.add(n_conta2);
		n_conta2.setColumns(10);
		
		JButton btnSacar = new JButton("Sacar");
		btnSacar.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnSacar.setBounds(11, 201, 89, 23);
		contentPane.add(btnSacar);
		
		btnSacar.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	sacar();
		    }
		});

		
		JButton btnDepositar = new JButton("Depositar");
		btnDepositar.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnDepositar.setBounds(110, 201, 89, 23);
		contentPane.add(btnDepositar);
		
		btnDepositar.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	depositar();
		    }
		});
		
		JButton bntTransferir = new JButton("Transferir");
		bntTransferir.setFont(new Font("Tahoma", Font.PLAIN, 10));
		bntTransferir.setBounds(304, 169, 89, 23);
		contentPane.add(bntTransferir);
		
		bntTransferir.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	transferir();
		    }
		});
		
		JLabel lblValor = new JLabel("Valor");
		lblValor.setBounds(110, 153, 46, 14);
		contentPane.add(lblValor);
		
		valor_2 = new JTextField();
		valor_2.setBounds(110, 170, 86, 20);
		contentPane.add(valor_2);
		valor_2.setColumns(10);
		
		conta_para = new JTextField();
		conta_para.setBounds(208, 170, 86, 20);
		contentPane.add(conta_para);
		conta_para.setColumns(10);
		
		JLabel lblContaPara = new JLabel("Conta para");
		lblContaPara.setBounds(208, 153, 86, 14);
		contentPane.add(lblContaPara);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 115, 414, 2);
		contentPane.add(separator);
		
		JLabel lblNewLabel = new JLabel("CONTAS");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel.setBounds(11, 11, 88, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblTransaes = new JLabel("TRANSA\u00C7\u00D5ES");
		lblTransaes.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblTransaes.setBounds(11, 128, 88, 14);
		contentPane.add(lblTransaes);
		
		JLabel label = new JLabel("N\u00BA da Conta");
		label.setHorizontalAlignment(SwingConstants.LEFT);
		label.setBounds(11, 29, 88, 14);
		contentPane.add(label);
		
		n_conta1 = new JTextField();
		n_conta1.setColumns(10);
		n_conta1.setBounds(11, 46, 86, 20);
		contentPane.add(n_conta1);
		
		valor_1 = new JTextField();
		valor_1.setColumns(10);
		valor_1.setBounds(110, 46, 86, 20);
		contentPane.add(valor_1);
		
		JLabel label_1 = new JLabel("Valor");
		label_1.setBounds(110, 29, 46, 14);
		contentPane.add(label_1);
		
		JButton btnCriar = new JButton("Criar");
		btnCriar.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnCriar.setBounds(208, 45, 89, 23);
		contentPane.add(btnCriar);
		
		btnCriar.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	criarConta();
		    }
		});
		
		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnExcluir.setBounds(110, 81, 89, 23);
		contentPane.add(btnExcluir);
		
		btnExcluir.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	excluirConta();
		    }
		});
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 235, 414, 2);
		contentPane.add(separator_1);
		
		table = new JTable(modelo);
		table.setBounds(11, 273, 413, 103);
		contentPane.add(table);		

        modelo.addColumn("numeroConta");
        modelo.addColumn("Saldo");
        modelo.addColumn("Tipo");
        
        JButton btnMostrar = new JButton("Mostrar");
		btnMostrar.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnMostrar.setBounds(11, 81, 89, 23);
		contentPane.add(btnMostrar);
		
		btnMostrar.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	mostrarConta();
		    }
		});
		
		JButton btnListarTodas = new JButton("Listar todas");
		btnListarTodas.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnListarTodas.setBounds(208, 81, 89, 23);
		contentPane.add(btnListarTodas);
		
		btnListarTodas.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	listarContas();
		    }
		});
		
		msg = new JLabel("");
		msg.setHorizontalAlignment(SwingConstants.CENTER);
		msg.setForeground(Color.BLUE);
		msg.setBounds(82, 248, 271, 14);
		contentPane.add(msg);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(0, 297, 434, -91);
		contentPane.add(scrollPane);
		
		Vector model = new Vector();
        model.addElement( new Item(1, "Corrente" ) );
        model.addElement( new Item(2, "Poupan�a" ) );
		
		comboBox = new JComboBox(model);
		comboBox.setSelectedIndex(0);
		comboBox.setBounds(306, 46, 87, 20);
		contentPane.add(comboBox);

	}
	
	private void mostrarMSG(String mensagem) {
		msg.setText(mensagem);
	}
	
	private void zerarTabela() {
		modelo.setRowCount(0);
	}
	
	public void criarLinha(ContaBancaria conta) {
		String tipoConta = "Corrente";
		if( conta instanceof ContaPoupanca )
			tipoConta = "Poupan�a";
		modelo.addRow(new Object[]{conta.getNumeroConta(), "R$ "+decimalFormat.format(conta.getSaldo()), tipoConta});
	}
	
	public void criarConta() {
	    try {
	    	long contaNum = Long.parseLong(n_conta1.getText());
	    	double valorConta = Double.parseDouble(valor_1.getText());
			if(banco.existeConta(contaNum)) {
				mostrarMSG("Conta j� existe");
			}else{
				Item item = (Item)comboBox.getSelectedItem();
				if(item.getId() == 1) {
					banco.inserir(new ContaCorrente(contaNum, valorConta));
				}else {
					banco.inserir(new ContaPoupanca(contaNum, valorConta));
				}
				mostrarMSG("Conta inserida");
				zerarTabela();
				criarLinha(banco.procurarConta(contaNum));
				n_conta1.setText("");
				valor_1.setText("");
			}
	    } catch (NumberFormatException nfe) {
	    	mostrarMSG("Dados incorreto");
	    }
	}
	
	public void mostrarConta() {
	    try {
	    	long contaNum = Long.parseLong(n_conta1.getText());
			if(banco.existeConta(contaNum)) {
				zerarTabela();
				criarLinha(banco.procurarConta(contaNum));
			}else{
		    	mostrarMSG("Conta n�o encontrada");
			}
	    } catch (NumberFormatException nfe) {
	    	mostrarMSG("Dados incorreto");
	    }
	}
	
	public void listarContas() {
		mostrarMSG("Lista de contas");
		zerarTabela();
		List<ContaBancaria> Contas = new ArrayList<ContaBancaria>();
		Contas = banco.getContas();
		for (ContaBancaria p: Contas) {
			criarLinha(p);
		}
	}
	
	public void excluirConta() {
	    try {
	    	long contaNum = Long.parseLong(n_conta1.getText());
			if(banco.existeConta(contaNum)) {
				zerarTabela();
				banco.remover(contaNum);
				mostrarMSG("Conta excluida!");
				n_conta1.setText("");
				valor_1.setText("");
			}else{
		    	mostrarMSG("Conta n�o encontrada");
			}
	    } catch (NumberFormatException nfe) {
	    	mostrarMSG("Dados incorreto");
	    }
	}
	/****************/
	/****************/
	public void sacar() {
	    try {
	    	long contaNum = Long.parseLong(n_conta2.getText());
	    	double valorDig = Double.parseDouble(valor_2.getText());
			if(banco.existeConta(contaNum)) {
				ContaBancaria p = banco.procurarConta(contaNum);
				if(p.getSaldo() < valorDig) {
					mostrarMSG("Saldo insuficiente");
				}else{
					zerarTabela();
					criarLinha(p);
					p.sacar(valorDig);
					criarLinha(p);
					mostrarMSG("Saque efetuado - "+"R$ "+decimalFormat.format(valorDig));
					n_conta2.setText("");
					valor_2.setText("");
				}
			}else{
		    	mostrarMSG("Conta n�o encontrada");
			}

	    } catch (NumberFormatException nfe) {
	    	mostrarMSG("Dados incorreto");
	    }
	}
	
	public void depositar() {
	    try {
	    	long contaNum = Long.parseLong(n_conta2.getText());
	    	double valorDig = Double.parseDouble(valor_2.getText());
			if(banco.existeConta(contaNum)) {
				ContaBancaria p = banco.procurarConta(contaNum);
				zerarTabela();
				criarLinha(p);
				p.depositar(valorDig);
				criarLinha(p);
				mostrarMSG("Dep�sito efetuado - "+"R$ "+decimalFormat.format(valorDig));
				n_conta2.setText("");
				valor_2.setText("");
			}else{
		    	mostrarMSG("Conta n�o encontrada");
			}
	    } catch (NumberFormatException nfe) {
	    	mostrarMSG("Dados incorreto");
	    }		
	}
	
	public void transferir() {
	    try {
	    	long contaNum = Long.parseLong(n_conta2.getText());
	    	long contaPara = Long.parseLong(conta_para.getText());
	    	double valorDig = Double.parseDouble(valor_2.getText());
			if(banco.existeConta(contaNum)) {
				if(banco.existeConta(contaPara)) {
					ContaBancaria p = banco.procurarConta(contaNum);
					if(p.getSaldo() < valorDig) {
						mostrarMSG("Saldo insuficiente");
					}else{
						ContaBancaria o = banco.procurarConta(contaPara);
						zerarTabela();
						criarLinha(p);
						p.sacar(valorDig);
						criarLinha(p);
						criarLinha(o);
						o.depositar(valorDig);
						criarLinha(o);
						mostrarMSG("Transfer�ncia realizada - "+"R$ "+decimalFormat.format(valorDig));
						n_conta2.setText("");
						valor_2.setText("");
					}
				}else{
			    	mostrarMSG("Conta destino n�o encontrada");
				}
			}else{
		    	mostrarMSG("Conta origem n�o encontrada");
			}
	    } catch (NumberFormatException nfe) {
	    	mostrarMSG("Dados incorreto");
	    }		
	}
}
class Item {
    private int id;
    private String description;

    public Item(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String toString() {
        return description;
    }
}