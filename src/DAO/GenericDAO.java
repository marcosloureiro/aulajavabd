package DAO;

import java.sql.SQLException;
import java.util.List;

public interface GenericDAO<T> {
    public int inserir(T objeto) throws SQLException;
    public int alterar(T objeto) throws SQLException;
    public int remover(T objeto) throws SQLException;
    public List<T> listarNome(String nome) throws SQLException;
    public List<T> listar() throws SQLException;
}
