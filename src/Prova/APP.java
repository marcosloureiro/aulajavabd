package Prova;
import java.sql.*;
import java.util.Scanner;

public class APP {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		Class.forName("com.mysql.jdbc.Driver");
		
		String strCon = "jdbc:mysql://localhost:3306/prova";

        Connection con = DriverManager.getConnection(strCon, "root", "root");
        
        Statement stmt = con.createStatement();
        
        String query = "INSERT INTO produtos (Descricao, Valor, Quantidade) VALUES (?, ?, ?)";
        PreparedStatement pstmt = con.prepareStatement(query);
        
        Scanner tecla = new Scanner(System.in);        

        int op;
        do {
	      System.out.println("");
          System.out.println("*** MENU PRINCIPAL ***");
          System.out.println("1-Incluir produto");
          System.out.println("2-Listar");
          System.out.println("3-Sair");
          op = tecla.nextInt();
          if(op == 1) {
      		System.out.println("Descricao do produto:");
            tecla.nextLine();
            String desc = tecla.nextLine();
            System.out.println("Digite o valor:");
            float valor = tecla.nextFloat();   
            System.out.println("Digite a quantidade:");
            int quant = tecla.nextInt();
            pstmt.setString(1, desc);
            pstmt.setFloat(2, valor);
            pstmt.setInt(3, quant);
            int result = pstmt.executeUpdate();
          }else if(op == 2){  	        
  	        String query2 = "SELECT Descricao, Valor, Quantidade, (Valor * Quantidade) AS Total FROM produtos";
  	        ResultSet rs = stmt.executeQuery(query2);
  	        System.out.println("PRODUTOS NA TABELA*****");
  	        while (rs.next()){
  	        	System.out.println(rs.getString("Descricao")
  	        			+ " | Valor: "+rs.getFloat("Valor")
  	        			+ " | Quantidade: "+rs.getInt("Quantidade")
  	        			+ " | Total: "+rs.getFloat("Total")
  	        	);
  	        	System.out.println("");
  	        }
          }
        } while (op!=3); 
	}
}
