package aulajavabd;
import java.sql.*;

public class Aplicativo {

	public static void main(String[] args) {
		
		try {
            
            // 1. Carregar o driver
            Class.forName("com.mysql.jdbc.Driver");
            
            // 2. Definir a URL de conex�o
            String strCon = "jdbc:mysql://localhost:3306/clinica";
            
            // 3. Estabelecer a conex�o
	        Connection con = DriverManager.getConnection(strCon, "root", "root");
            
            //4. Criar objeto do tipo statement
            Statement stmt = con.createStatement();
            
            //5. Executar uma consulta
            int resultado = stmt.executeUpdate("INSERT INTO Medicos VALUES (6, 'Jorge', 42, 'neuro cirurgi�o', 10000100001, 'Rio de Janeiro', null);");
            
            if(resultado > 0) {
            	System.out.println("Inserido com sucesso!");
            }
            ResultSet rs = stmt.executeQuery("SELECT * FROM Medicos");
            //6. Processar resultado
            
            while (rs.next()){
                  System.out.println("Nome: " + rs.getString("nome") 
                  + " - " + "Idade: " + rs.getShort("idade"));
            }
            
            //7. Fechar conex�o
            con.close();
            rs.close();
            stmt.close();
            
        } catch (ClassNotFoundException ex1) {
            System.out.println("Classe n�o encontrada");
        } catch (com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException ex2) {
        	System.out.println("Medico ja cadastrado!");
        } catch (SQLException ex3) {
        	System.out.println(ex3);
        }
	}
}
